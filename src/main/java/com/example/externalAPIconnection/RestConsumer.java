package com.example.externalAPIconnection;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Scanner;

public class RestConsumer {

    public void getUsersAsJson() {
        RestTemplate restTemplate = new RestTemplate();

        int counter = 0;
        while(counter == 0){
            String resourceUrl2 = "http://api.nbp.pl/api/exchangerates/rates/{table}/{code}/";
            Scanner scanner = new Scanner(System.in);
            System.out.print("Give the table name [a or b]: ");
            String table = scanner.nextLine();
            System.out.println("Give the currency: ");
            String currencyCode = scanner.nextLine();

            ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl2, String.class, table,currencyCode);
            String productsJson = response.getBody();
            System.out.println(productsJson);

            System.out.println("Do you want to give another parameters: [yes=1 or no=0]");
            int temp = scanner.nextInt();
            if(temp == 0){
                counter=1;
            }
            System.out.print("Good bye!");

        }

        //String resourceUrl = "http://localhost:8080/users";
        //String resourceUrl = "http://api.nbp.pl/api/exchangerates/tables/A/2022-11-21/";
        //String resourceUrl = "http://api.nbp.pl/api/exchangerates/rates/A/EUR/today/";
        //String resourceUrl2 = "http://api.nbp.pl/api/exchangerates/rates/{table}/{code}/";
        //String table = "a";
        //String currencyCode = "EUR";


        // Fetch JSON response as String wrapped in ResponseEntity

        //ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);
        //ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl2, String.class, table,currencyCode);
        //String productsJson = response.getBody();
        //System.out.println(productsJson);


    }
}
